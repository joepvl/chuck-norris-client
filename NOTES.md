Some comments on the requirements and my interpretation of them, point by point:

## Mandatory requirements

> 1. Fetch 10 Random Chuck Norris jokes from the following API:
>    http://api.icndb.com/jokes/random/10;

_I interpreted this as "do this on every page load"._

> 2. The jokes need to be displayed in a list;

_This one's clear._

> 3. In this list we can mark certain jokes as favorite. The favorites jokes
>    will appear in a favorites list with a max of 10 (unique) items;

_Also clear._

> 4. There should be an option to remove jokes from the favorites list as well;

_Same here._

> 5. On refresh the favorites lists should be maintained, maybe it’s a good idea
>    to use the database and/or caching for this;

_I disagree; at this point user identity hasn't come into play, so why store
favorites in the database? It's ephemeral data. I used localStorage for this one._

> 6. We can turn on/off a timer via a button (every 5 seconds) which will add
>    one random joke to the favorites list that is fetched from
>    http://api.icndb.com/jokes/random/1 until the list has 10 items.

_Challenging but still compact._

## Optional requirements

As mentioned I didn't have time to implement these, but here's how I would have:

> 1. Create a login page that will log you in via the JWT-functionality of the API- Gateway;

_I interpret this to mean this page should be a gateway to the other actual
jokes page. There's no mention of a registration feature, so I'd use a user
that's already in the db. The only one there is the admin user but that one's
password doesn't abide by the security restrictions mentioned in optional
requirement 5, so I'd change the password to a valid one (e.g. `abcaabb`) in
`config/dev.js`, delete the user from the command line (`DELETE FROM Users WHERE username = 'admin'`), and then restart the server to recreate it with the right
password. So now we can technically log in. The login page is a component with a
two-field form that gets rendered if the user isn't logged in (see point 3
below). It makes an async POST request to `/login` with user/password in the
request body when the form is submitted with a valid-looking password._

> 2. Store the JWT-Token in the browser;

_I know this is a contested issue, but I'd store it in localStorage because
there's no security risk (XSS or otherwise) in this case._

> 3. If a valid JWT-Token is stored in the browser, auto-login the user;

_I'd use the `login/verify` endpoint for this; if it returns `{ "msg": "authorized" }` that means the user is authorized. As mentioned, no router
shenanigans on the client, so it's a matter of wrapping the page in a component
that renders a "logging in..." type message while checking the token's validity,
the login component if there's no token or it's not valid, or the actual joke
app if the token is valid._

> 4. Display only the stored jokes & favorites for the logged in user;

_This requires some pretty major changes on both client and server, of which
I'll give a rough overview. I'd write a migration to create a `Jokes` table
indexed by id, and also a `UsersJokes` join table (with UserId and JokeId
columns) since it's a many-to-many relationship. I'd modify the `/jokes` route
to save any incoming jokes passing through to the db using a new `Jokes` model.
There should also be a Favorites model/table and the same goes for
UsersFavorites. To add or remove favorites for a user, I'd `POST` to
`/users/:id/favorites` and handle the db updates in that route.
`/users/:id/jokes` would represent the random jokes in the list. `GET`ing these
routes would yield the current jokes and favorites for the user; I'd return the
full jokes from the server, not just their `id`s. On the client, I might use
`normalizr` with redux to normalize these responses and manage the data that
way._

> 5. The login form should consist of a username/email field (both can be used
>    to login at the login API) and password which must comply to the following
>    password security requirements:
>    a. Passwords must include one increasing straight of at least three
>    letters, like ‘abc’, ‘cde’, ‘fgh’, and so on, up to ‘xyz’.
>    b. They cannot skip letters so e.g. ‘acd’ doesn't count.
>    c. Passwords may not contain the letters i, O, or l, as these letters can be
>    mistaken for other characters and are therefore confusing.
>    d. Passwords must contain at least two non-overlapping pairs of letters, like aa,
>    bb, or cc.
>    e. Passwords cannot be longer than 32 characters.

_Because there is no registration functionality, we only need to check and
enforce these rules on the client side. This is a matter of validating the
string in the password field and reporting the results back to the user if there
are any errors. I'd use the `v8n` package for this. I actually wrote the
validation code — see below. You can also find & run it at
https://runkit.com/embed/0pmoqdijno7f ._

```js
var v8n = require("v8n")

v8n.extend({
    includesNonOverlappingLetterPairs: (input=1) => str => {
        let pairs = [];

        for (let i=1, l=str.length; i<l; i++) {
          const prevChar = str[i - 1];
          const char = str[i];
          if (prevChar === char) {
            pairs.push(prevChar + char);
            if (pairs.length === input) {
              return true;
            }
            // skip to the next one; we don't want to count overlapping pairs (e.g. 'aaa')
            i++;
          }
        }

        return false;
    }
});

const threeLetterSeqs = [
  'abc', 'bcd', 'cde', 'def',
  'efg', 'fgh', 'ghi', 'hij',
  'ijk', 'jkl', 'klm', 'lmn',
  'mno', 'nop', 'opq', 'pqr',
  'qrs', 'rst', 'stu', 'uvw',
  'vwx', 'wxy', 'xyz'
];

const validator = v8n()
    .string()
    .pattern(new RegExp(threeLetterSeqs.join('|')))
    .pattern(/[^iOl]/)
    .includesNonOverlappingLetterPairs(2)
    .maxLength(32);

const errors = validator.testAll('aabbcd');

console.log(errors.map(err => err.rule));
```
