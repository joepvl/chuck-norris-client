import { useState, useCallback } from "react";

export default function useLocalStorageState(initial, key) {
  const [state, setState] = useState(() => {
    let st = JSON.parse(localStorage.getItem(key));
    if (st === null) st = initial;
    return st;
  });

  const setItem = useCallback(
    item => {
      setState(item);
      localStorage.setItem(key, JSON.stringify(item));
    },
    [key]
  );

  const clear = useCallback(() => localStorage.setItem(key, null), [key]);

  return [state, setItem, clear];
}
