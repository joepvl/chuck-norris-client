export const keyBy = (list, indexKey = "id") =>
  list.reduce((acc, item) => {
    acc[item[indexKey]] = item;
    return acc;
  }, {});
