import axios from "axios";

const BASE_URL = "http://localhost:3000";
const API = axios.create({
  baseURL: BASE_URL
});

export default {
  getJoke(id) {
    return API.get(`/jokes/${id}`).then(res => res.data.value);
  },

  getRandomJokes(amount) {
    return API.get(`/jokes/random/${amount}`).then(res => res.data.value);
  }
};
