import React from "react";
import "./SwitchButton.scss";

const SwitchButton = ({ on, onText, offText, onClick }) => (
  <button
    className={["SwitchButton", on ? "is-on" : "is-off"].join(" ")}
    onClick={onClick}
  >
    {on ? onText : offText}
  </button>
);

export default SwitchButton;
