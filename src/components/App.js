import React, { useEffect, useReducer, useState } from "react";
import "./App.scss";
import API from "../lib/API";
import { keyBy } from "../lib/util";
import useInterval from "../lib/useInterval";
import useLocalStorageState from "../lib/useLocalStorageState";
import FavesContext from "../state/FavesContext";

import JokesList from "./JokesList";
import SwitchButton from "./SwitchButton";

const types = {
  ADD_FAVE: "ADD_FAVE",
  ADD_FAVES: "ADD_FAVES",
  TOGGLE_FAVE: "TOGGLE_FAVE"
};

const lsKeys = {
  faves: "faves",
  allJokes: "allJokes"
};

function favesReducer(state, action) {
  switch (action.type) {
    case types.ADD_FAVE: {
      const { id } = action;
      return state.includes(id)
        ? state
        : state.length === 10
        ? state
        : [...state, id];
    }
    case types.ADD_FAVES: {
      const { ids } = action;
      return [...state, ...ids];
    }
    case types.TOGGLE_FAVE: {
      const { id } = action;
      return state.includes(id)
        ? state.filter(faveId => faveId !== id)
        : state.length === 10
        ? state
        : [...state, id];
    }
    default:
      return state;
  }
}

export default function App() {
  const [allJokes, setAllJokes, clearAllJokes] = useLocalStorageState(
    {},
    lsKeys.allJokes
  );
  const [isFetchingRandomJokes, setIsFetchingRandomJokes] = useState(true);
  const [randomJokes, setRandomJokes] = useState([]);
  const [faves, favesDispatch] = useReducer(favesReducer, []);
  const [isTimerActive, setIsTimerActive] = useState(false);

  const favesLSDispatch = action => {
    const nextFaves = favesReducer(faves, action);
    localStorage.setItem(lsKeys.faves, JSON.stringify(nextFaves));
    favesDispatch(action);
  };

  const faveJokes = faves.reduce((acc, fave) => {
    acc.push(allJokes[fave]);
    return acc;
  }, []);

  useEffect(() => {
    setIsFetchingRandomJokes(true);
    API.getRandomJokes(10)
      .then(jokes => {
        setAllJokes({ ...allJokes, ...keyBy(jokes, "id") });
        setRandomJokes(jokes);
        setIsFetchingRandomJokes(false);
      })
      .catch(err => {
        setFetchError(err.message);
      });

    const favesFromLS = JSON.parse(localStorage.getItem(lsKeys.faves));
    if (favesFromLS) {
      favesDispatch({ type: types.ADD_FAVES, ids: favesFromLS });
    }
  }, []);

  useInterval(
    () => {
      if (faves.length <= 10) {
        // I'm going to assume it is a new joke. If it isn't, better luck next
        // time the timer goes off.
        API.getRandomJokes(1).then(([joke]) => {
          setAllJokes({ ...allJokes, [joke.id]: joke });
          favesLSDispatch({ type: types.ADD_FAVE, id: joke.id });
        });
      }
    },
    isTimerActive && faves.length <= 10 ? 5000 : null
  );

  const toggleFav = id => favesLSDispatch({ type: types.TOGGLE_FAVE, id });
  const toggleTimer = () => setIsTimerActive(!isTimerActive);

  return (
    <FavesContext.Provider value={{ faves, toggleFav }}>
      <div className="App">
        <h1 className="App-title">
          <a href="https://knowyourmeme.com/memes/chuck-norris-facts">
            COME GITCHER CHUCK NORRIS JOKES
          </a>
        </h1>
        {!isFetchingRandomJokes && (
          <SwitchButton
            on={isTimerActive}
            onText="Stop adding a fav joke every 5 seconds"
            offText="Add a fav joke every 5 seconds!"
            onClick={toggleTimer}
          />
        )}
        <div className="App-jokesLists">
          <>
            <JokesList
              jokes={randomJokes}
              title={"Random jokes"}
              placeholder="Fetching random jokes..."
            />
            <JokesList
              jokes={faveJokes}
              title={`Your favorite jokes (${faves.length}/10)`}
              placeholder="Star some jokes to add them to this list!"
            />
          </>
        </div>
      </div>
    </FavesContext.Provider>
  );
}
