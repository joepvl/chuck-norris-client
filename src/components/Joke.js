import React, { useContext } from "react";
import "./Joke.scss";

import FavesContext from "../state/FavesContext";

const StarToggle = ({ id, on, onChange }) => (
  <>
    <input
      id={`StarToggle-${id}`}
      className="StarToggle-checkbox"
      type="checkbox"
      checked={on}
      onChange={onChange}
    />
    <label className="StarToggle-label" htmlFor={`StarToggle-${id}`} />
  </>
);

export default function Joke({ joke = {}, isFav = false }) {
  const { toggleFav } = useContext(FavesContext);
  const checkboxId = `fav-${joke.id}`;

  return (
    <li className="Joke">
      <div className="Joke-text">{joke.joke}</div>
      <StarToggle id={joke.id} on={isFav} onChange={() => toggleFav(joke.id)} />
    </li>
  );
}
