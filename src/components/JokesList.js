import React, { useContext } from "react";
import Joke from "./Joke";
import FavesContext from "../state/FavesContext";

import "./JokesList.scss";

export default function JokesList({
  jokes = [],
  title = "List of Jokes",
  placeholder = "Get some jokes!"
}) {
  const { faves } = useContext(FavesContext);

  return (
    <div className="JokesList">
      <h2 className="JokesList-title">{title}</h2>
      <ul className="JokesList-list">
        {jokes.length ? (
          jokes.map(joke =>
            joke ? (
              <Joke joke={joke} isFav={faves.includes(joke.id)} key={joke.id} />
            ) : (
              <li>"No joke"</li>
            )
          )
        ) : (
          <li className="JokesList-placeholder">{placeholder}</li>
        )}
      </ul>
    </div>
  );
}
