import { createContext } from "react";

const FavesContext = createContext([]);

export default FavesContext;
